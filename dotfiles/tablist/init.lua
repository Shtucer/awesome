require("awful.titlebar")
require("unicode")
--require("skype.handler")
local capi =
{ 
    client = client,
    awesome = awesome,
    wibox = wibox,
    widget = widget
}
local layout = require("awful.widget.layout")
local widget = require("awful.widget")
local wibox = require("awful.wibox")
local client = require("awful.client")
local awful = require("awful")
local util = require("awful.util")
local table = table
local clientkeys = shifty.config.clientkeys
local beautiful = require("beautiful")
local setmetatable = setmetatable
--local metatable = metatable
--local titlebar = require("awful.titlebar")
local print = print
local ipairs = ipairs
local utf8 = unicode.utf8
local common = awful.widget.common


module("tablist")

tabs = {}







function is_handled(client, tablist)
    for i,c in ipairs(tablist) do
        if c == client then
            return true, i
        end
    end
    return false, 0
end

function new(c, name, args)


    if not name then name = "Tabs" .. #tabs+1 end
    if not tabs[name] then tabs[name] = {} end
    if not args then args={} end
    if not args.height then args.height = 16 end
    if is_handled(c, tabs[name]) then print("Handled"); return end

    
    hide_all(tabs[name])

-- Keybinding for tab switch
-- [Ctrl-Tab] - next tab
-- [Ctrl-Shift-Tab] - prev tab
-- TODO: conflicts with some clients
    key_tab = awful.util.table.join(clientkeys,
    awful.key({"Control",  }, "Tab", function(c) next(c, tabs[name]) end),
    awful.key({"Control",  "Shift"}, "Tab", function(c) prev(c, tabs[name]) end)
    )

    for i=0,9 do
        key_tab = awful.util.table.join(key_tab,
            awful.key({"Control", }, i, function(c) switch_to(c, i, tabs[name]) end)
        )
    end


    c:keys(key_tab)

    local data = setmetatable({}, { __mode = 'kv' })
    local u = function() label(c, c.screen, tabs[name]) end
    capi.client.add_signal("new", function (c)
        c:add_signal("property::urgent", u)
        c:add_signal("property::floating", u)
        c:add_signal("property::maximized_horizontal", u)
        c:add_signal("property::maximized_vertical", u)
        c:add_signal("property::minimized", u)
        c:add_signal("property::name", u)
        c:add_signal("property::icon_name", u)
        c:add_signal("property::icon", u)
        c:add_signal("property::skip_taskbar", u)
        c:add_signal("property::screen", u)
        c:add_signal("property::hidden", u)
        c:add_signal("tagged", u)
        c:add_signal("untagged", u)
    end)
    capi.client.add_signal("unmanage", function(c) unhandle(c, tabs[name]);u() end)
    capi.client.add_signal("list", u)
    capi.client.add_signal("focus", u)
    capi.client.add_signal("unfocus", u)
    u()

    table.insert(tabs[name], c)
    
    tlist = capi.wibox(args)
    
    -- TODO: Maybe change tasklist widget to textbox?
    local task = awful.widget.tasklist(function(c)
        return label(c, c.screen, tabs[name])
    end)
    tlist.widgets = {
        task,
        layout = layout.horizontal.leftright
    }

    c.titlebar = tlist
    c.hidden = false
    print(#tabs[name])
end


-- Remove client from tablist when killed
-- (signal "unmanage")
function unhandle(client, tablist)
    for k,c in ipairs(tablist) do
        if c == client then
            prev(client, tablist)
            tablist[k] = nil
            table.remove(tablist, k)
            break
        end
    end
end

function hide_all(tablist)
    for k,c in ipairs(tablist) do
        if c then c.hidden = true
        else
            table.remove(tablist, k)
        end
    end

end


function label(client, screen, tablist)
    if not is_handled(client, tablist) then return end

    local theme = beautiful.get()
    local fg_focus = theme.tasklist_fg_focus or theme.fg_focus
    local bg_focus = theme.tasklist_bg_focus or theme.bg_focus
    local fg_urgent = theme.tasklist_fg_urgent or theme.fg_urgent
    local bg_urgent = theme.tasklist_bg_urgent or theme.bg_urgent
    local fg_minimize = theme.tasklist_fg_minimize or theme.fg_minimize
    local bg_minimize = theme.tasklist_bg_minimize or theme.bg_minimize
    local floating_icon = theme.tasklist_floating_icon
    local font = theme.tasklist_font or theme.font or "Ubuntu 10"
--    local font = theme.font or "Ubuntu 10"
    local bg = nil

    local text = "<span font_desc='"..font.."'>"
    local name
    print(client.name)
    for k,c in ipairs(tablist) do
        name = util.escape(c.name) or util.escape("<untitled>")
        if utf8.len(name) > 25 then
            name = utf8.sub(name, 1, 22) .. "..."
        end
        --        name = name .. "&nbsp;"
        name = "  [ " .. k .. " ] - " .. name
        if not c.hidden then
            --            bg = bg_focus
            text = text .. "<span bgcolor='" .. util.color_strip_alpha(bg_focus) .. "' color='"..util.color_strip_alpha(fg_focus).."'>"..name.."</span>"
        elseif c.urgent then
            --            bg = bg_urgent
            text = text .. "<span bgcolor='" .. util.color_strip_alpha(bg_urgent) .. "' color='"..util.color_strip_alpha(fg_urgent).."'>"..name.."</span>"
        else
            text = text .. "<span bgcolor='" .. util.color_strip_alpha(theme.bg_normal) .. "' color='" .. util.color_strip_alpha(theme.fg_normal) .. "'>" .. name .. "</span>"
        end
    end
    text = text .. "</span>"
--    print(text)
    return text --, nil, nil, nil
end


-- Remove orphans from tablist
function clean(tablist)
    idx = {}
    for k,c in ipairs(tablist) do
        if not c then
            table.insert(idx,k)
        end
    end
    for i=1,#idx do
        table.remove(chats, idx[i])
    end
end


function switch_to(client, n, tablist)
    print("switch to: " .. n)
    if n == 0 then n = 10 end
    if n > #tablist then return end
    
    client.hidden = true
    tablist[n].hidden = false
    capi.client.focus = tablist[n]

end

function prev(client, tablist)
    clean(tablist)
    if #tablist == 1 then return end
    current = 1
    for i,c in ipairs(tablist) do
        if c == client then 
            current = i - 1
            break
        end
    end
    if current < 1 then
        current = #tablist
    end

    client.hidden = true
    tablist[current].hidden = false
    capi.client.focus = tablist[current]
        
end



function next(client, tablist)
    clean(tablist)
    if #tablist == 1 then return end
    current = 1
    for i,c in ipairs(tablist) do
        if c == client then 
            current = i + 1
            break
        end
    end
    if current > #tablist then
        current = 1
    end

    client.hidden = true
    tablist[current].hidden = false
    capi.client.focus = tablist[current]
        
end



-- Deprecated
function add(c, args)
    --    if not c or (c.type ~= "normal" and c.type ~= "dialog") then return end
    if not args then args = {} end
    if not args.height then args.height = capi.awesome.font_height end
    if not args.widgets then return false end
--    print("Tablist add to " .. c.name)
    local tb = capi.wibox(args)
--[[
    local tablist_widget = capi.widget({type = "textbox"})

    tablist_widget.text = c.name
    tablist_widget.width = 250
    local tablist_widget = tasklist(function(c) return skype.handler.label() end)    
]]
    tb.widgets = {
        args.widgets,
        layout = layout.horizontal.leftright
    }
    c.titlebar = tb
    print(c.titlebar)
end

function remove(c)
    print("Tablist remove from " .. c.name)

    c.titlebar = nil
end

-- Add client to named tablist
-- @param c The client
-- @param tablist Name of tablist
function add_to_tablist(c, tablist)
    print(c.name .. " add to tablist")
    if not c.titlebar then add(c) end
    c.skip_taskbar = true
end
