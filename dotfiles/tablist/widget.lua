

local capi =
{
    awesome = awesome,
    widget = widget,
    client = client
}


module("tablist.widget")


tabs = {}

key_tab = awful.util.table.join(clientkeys,
    awful.key({"Control",  }, "Tab", function(c) next(c) end),
    awful.key({"Control",  "Shift"}, "Tab", function(c) prev(c) end),
    awful.key({}, "Escape", function(c) unhandle(c) end),
    awful.key({"Control",}, "w", function(c) unhandle(c) end),
    awful.key({modkey, "Shift"}, "c", function(c) unhandle(c) end)

)


function update_tabs()
end

function new(c, name, args)
    if not name       then name = "Tabs" .. #tabs+1 end
    if not tabs[name] then tabs[name]={} end
    if not args       then args={} end

    if not tabs[name].c then tabs[name].c = c end

    c:keys(key_tab) 


end


function create(name)
end

function get(name)
end
