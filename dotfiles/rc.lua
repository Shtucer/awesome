package.path=package.path .. ";/home/shtucer/.config/awesome/conf.d/?.lua"
package.cpath=package.cpath..";/home/shtucer/.config/awesome/?.so"
-- Standard awesome library
require("awful")
require("awful.autofocus")
require("awful.rules")
-- Theme handling library
require("beautiful")
-- Notification library
require("naughty")
require("vicious")
require("shifty-new")

require("tablist")
require("qterm")
dofile(awful.util.getdir("config") .. "/conf.d/functions.lua")
-- Load Debian menu entries
--
require("debian.menu")
-- {{{ Variable definitions
-- Themes define colours, icons, and wallpapers
beautiful.init("/home/shtucer/.config/awesome/themes/zenburn/theme.lua")

-- This is used later as the default terminal and editor to run.
terminal = "uxterm -e byobu -R"
-- editor = os.getenv("EDITOR") or "editor"
editor = "gvim"
editor_cmd = editor

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
modkey = "Mod4"

-- Table of layouts to cover with awful.layout.inc, order matters.
layouts =
{
    awful.layout.suit.max,
    awful.layout.suit.floating,
    awful.layout.suit.tile,
    awful.layout.suit.tile.left,
    awful.layout.suit.tile.bottom,
    awful.layout.suit.tile.top,
    awful.layout.suit.fair,
    awful.layout.suit.fair.horizontal,
--    awful.layout.suit.spiral,
--    awful.layout.suit.spiral.dwindle,
--    awful.layout.suit.max.fullscreen,
--    awful.layout.suit.magnifier
}
-- }}}


dofile(awful.util.getdir("config") .. "/conf.d/kbdd.lua")

dofile(awful.util.getdir("config") .. "/conf.d/tags.lua")

dofile(awful.util.getdir("config") .. "/conf.d/menu.lua")

dofile(awful.util.getdir("config") .. "/conf.d/panels.lua")

dofile(awful.util.getdir("config") .. "/conf.d/keybindings.lua")


client.add_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.add_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)

dofile(awful.util.getdir("config") .. "/conf.d/autostart.lua")
