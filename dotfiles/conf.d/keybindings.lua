require("skype.handler")
last = "0"
-- {{{ Mouse bindings
root.buttons(awful.util.table.join(
    awful.button({ }, 3, function () mymainmenu:toggle() end),
    awful.button({ }, 4, awful.tag.viewnext),
    awful.button({ }, 5, awful.tag.viewprev)
))
-- }}}
-- {{{ Key bindings
globalkeys = awful.util.table.join(
    awful.key({ modkey,           }, "Left",   awful.tag.viewprev       ),
    awful.key({ modkey,           }, "Right",  awful.tag.viewnext       ),
    awful.key({ modkey,           }, "Escape", awful.tag.history.restore),

    awful.key({ modkey,           }, "Cyrillic_o",
        function ()
            awful.client.focus.byidx( 1)
            if client.focus then client.focus:raise() end
        end),

    awful.key({ modkey,           }, "j",
        function ()
            awful.client.focus.byidx( 1)
            if client.focus then client.focus:raise() end
        end),

    awful.key({ modkey,           }, "Cyrillic_el",
        function ()
            awful.client.focus.byidx(-1)
            if client.focus then client.focus:raise() end
        end),
    awful.key({ modkey,           }, "k",
        function ()
            awful.client.focus.byidx(-1)
            if client.focus then client.focus:raise() end
        end),
    awful.key({ modkey,           }, "w", function () mymainmenu:show({keygrabber=true}) end),

    -- Layout manipulation
    awful.key({ modkey, "Shift"   }, "j", function () awful.client.swap.byidx(  1)    end),
    awful.key({ modkey, "Shift"   }, "k", function () awful.client.swap.byidx( -1)    end),
    awful.key({ modkey, "Control" }, "j", function () awful.screen.focus_relative( 1) end),
    awful.key({ modkey, "Control" }, "k", function () awful.screen.focus_relative(-1) end),
    awful.key({ modkey,           }, "u", awful.client.urgent.jumpto),
    awful.key({ modkey,           }, "Tab",
        function ()
            awful.client.focus.history.previous()
            if client.focus then
                client.focus:raise()
            end
        end),

    -- Standard program
    awful.key({ modkey,           }, "Return", function () awful.util.spawn(terminal) end),
    awful.key({ modkey,           }, "e", function() awful.util.spawn("nautilus --no-desktop") end),
    awful.key({ modkey, "Control" }, "r", awesome.restart), 
    awful.key({ modkey, "Shift"   }, "q", awesome.quit),

    awful.key({ modkey,           }, "l",     function () awful.tag.incmwfact( 0.05)    end),
    awful.key({ modkey,           }, "h",     function () awful.tag.incmwfact(-0.05)    end),
    awful.key({ modkey, "Shift"   }, "h",     function () awful.tag.incnmaster( 1)      end),
    awful.key({ modkey, "Shift"   }, "l",     function () awful.tag.incnmaster(-1)      end),
    awful.key({ modkey, "Control" }, "h",     function () awful.tag.incncol( 1)         end),
    awful.key({ modkey, "Control" }, "l",     function () awful.tag.incncol(-1)         end),
    awful.key({ modkey,           }, "space", function () awful.layout.inc(layouts,  1) end),
    awful.key({ modkey, "Shift"   }, "space", function () awful.layout.inc(layouts, -1) end),

    -- Prompt
--    awful.key({ modkey },            "r",     function () mypromptbox[mouse.screen]:run() end),
    awful.key({ modkey }, "`", function() dropdown_toggle ("xterm -name terminal", 600) end),
    awful.key({ "Control" }, "space", function() mypromptbox[mouse.screen]:run() end),
    awful.key({  "Mod1"  }, "Shift_L",     function () 
        print("Switch")
        os.execute(kbd_dbus_next_cmd) 
    end),
--    awful.key({ "Mod1" }, "Shift_L", function () kbdcfg.switch() end),
    awful.key({ modkey },            "r",     function () run_once("gmrun") end),
    awful.key({ "Control", },    "Escape", function() mymainmenu:toggle() end),
    awful.key({ modkey }, "x",
              function ()
                  awful.prompt.run({ prompt = "Run Lua code: " },
                  mypromptbox[mouse.screen].widget,
                  awful.util.eval, nil,
                  awful.util.getdir("cache") .. "/history_eval")
              end),
--{{{ Mixer an Music Player Daemon (mpd)
    awful.key({         },   "XF86AudioMedia", function() run_once("sonata") end),
    awful.key({         },   "XF86AudioRaiseVolume", function() awful.util.spawn("amixer -c 0 sset Master 5dB+") end),
    awful.key({         },   "XF86AudioLowerVolume", function() awful.util.spawn("amixer -c 0 sset Master 5dB-") end),
    awful.key({         },   "XF86AudioMute", 
    function() 
--        awful.util.spawn("amixer sset Master toggle")
       limits = io.popen("amixer -c 0 get Master | awk '/Limits:/ {print($3);print($5)}'")
       cur = io.popen("amixer -c 0 get Master | awk '/Front Left:/ {print($4)}'")
       for c in cur:lines() do
           if c == "0" then
               awful.util.spawn("amixer -c 0 sset Master "..last.."db")
           else
               last = c
               awful.util.spawn("amixer -c 0 sset Master 0")
           end
       end
    end),
    awful.key({         },   "XF86AudioPlay", function() awful.util.spawn("mpc toggle") end),
    awful.key({         },   "XF86AudioNext", function() awful.util.spawn("mpc next") end),
    awful.key({         },   "XF86AudioPrev", function() awful.util.spawn("mpc prev") end),
    awful.key({         },   "XF86AudioStop", function() awful.util.spawn("mpc stop") end)
    --}}}
    )
    clientkeys = awful.util.table.join(
--    awful.key({"Control"}, "Tab", function(c)
--
--            if string.find(c.name, "Skype™.*чат") then 
--                skype.handler.next(c) 
--            else 
--                print("Not Skype")
--                return true 
--            end 
--
--    end),

    awful.key({ modkey, "Control" }, "t", function(c)
        -- require("awful.titlebar")
        if not c.titlebar then
            mytitlebartitle = widget({ type = "textbox", name = "mytitlebartitle" })
            mytitlebartitle.text = "Fuck!"

            mytitlebar = tablist.add(c,{})
        else
            tablist.remove(c)
            --mytitlebar:widgets({ mytitlebartitle })
            --mytitlebar.add(c)
        end
    end),
    --   awful.key({ modkey, "Control" }, "0",      function(c) c.hidden = not c.hidden end),
    awful.key({ modkey,           }, "f",      function (c) c.fullscreen = not c.fullscreen  end),
    awful.key({ modkey, "Shift"   }, "c",      function (c) c:kill()                         end),
    awful.key({ modkey, "Control" }, "space",  awful.client.floating.toggle                     ),
    awful.key({ modkey, "Control" }, "Return", function (c) c:swap(awful.client.getmaster()) end),
    awful.key({ modkey,           }, "o",      awful.client.movetoscreen                        ),
    awful.key({ modkey, "Shift"   }, "r",      function (c) c:redraw()                       end),
    awful.key({ modkey,           }, "t",      function (c) c.ontop = not c.ontop            end),
    awful.key({ modkey,           }, "n",      function (c) c.minimized = not c.minimized    end),
    awful.key({ modkey,           }, "m",
    function (c)
        c.maximized_horizontal = not c.maximized_horizontal
        c.maximized_vertical   = not c.maximized_vertical
    end)
    )

    -- Compute the maximum number of digit we need, limited to 9
    keynumber = 0
    --for s = 1, screen.count() do
    --   keynumber = math.min(9, math.max(#tags[s], keynumber));
    --end

    for i=1,  9  do

        globalkeys = awful.util.table.join(globalkeys, awful.key({ modkey }, i,
        function ()
            local t = awful.tag.viewonly(shifty.getpos(i))
        end))
        globalkeys = awful.util.table.join(globalkeys, awful.key({ modkey, "Control" }, i,
        function ()
            local t = shifty.getpos(i)
            t.selected = not t.selected
        end))
        globalkeys = awful.util.table.join(globalkeys, awful.key({ modkey, "Control", "Shift" }, i,
        function ()
            if client.focus then
                awful.client.toggletag(shifty.getpos(i))
            end
        end))
        -- move clients to other tags
        globalkeys = awful.util.table.join(globalkeys, awful.key({ modkey, "Shift" }, i,
        function ()
            if client.focus then
                local t = shifty.getpos(i)
                awful.client.movetotag(t)
                awful.tag.viewonly(t)
            end
        end))
        -- globalkeys = awful.util.table.join(globalkeys,
        --     awful.key({ modkey }, i,
        --               function ()
        --                     local screen = mouse.screen
        --                     if tags[1][i] then
        --                         awful.tag.viewonly(tags[screen][i])
        --                     end
        --               end),
        --     awful.key({ modkey, "Control" }, "#" .. i + 9,
        --               function ()
        --                   local screen = mouse.screen
        --                   if tags[screen][i] then
        --                       awful.tag.viewtoggle(tags[screen][i])
        --                   end
        --               end),
        --     awful.key({ modkey, "Shift" }, "#" .. i + 9,
        --               function ()
        --                   if client.focus and tags[client.focus.screen][i] then
        --                       awful.client.movetotag(tags[client.focus.screen][i])
        --                   end
        --               end),
        --     awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
        --               function ()
        --                   if client.focus and tags[client.focus.screen][i] then
        --                       awful.client.toggletag(tags[client.focus.screen][i])
        --                   end
        --               end))

    end

    clientbuttons = awful.util.table.join(
    awful.button({ }, 1, function (c) client.focus = c; c:raise() end),
    awful.button({ modkey }, 1, awful.mouse.client.move),
    awful.button({ modkey }, 3, awful.mouse.client.resize)
    )


    awful.rules.rules = { rule = {}, properties = {buttons = clientbuttons}}

    root.keys(globalkeys)
    shifty.config.globalkeys = globalkeys
    shifty.config.clientkeys = clientkeys
    shifty.config.clientbuttons = clientbuttons
