shifty.config.tags = {
   ["dev"] = { init = true, position = 1, layout = awful.layout.suit.max                                        },
   ["web"] = { init = true, position = 2, layout = awful.layout.suit.max  },
   ["p2p"] = { init = true, position = 3               },
   ["gimp"] = { position = 5,init = true, layout = awful.layout.suit.tile.left, mwfact = 0.18, icon="/usr/share/icons/hicolor/16x16/apps/gimp.png", icon_only = true },
   ["im"] = { position = 4, init = true, layout = awful.layout.suit.tile.right, nmaster = 1, mwfact = 0.20},
   ["6"] = { position = 6, init = true},
   ["7"] = { position = 7, init = true},
   ["8"] = { position = 8, init = true},
   ["mm"] = { position = 9, init = true, layout = awful.layout.suit.max, mwfact = 0.18  },
}


dofile(awful.util.getdir("config") .. "/conf.d/rules.lua")

shifty.init()

--tags = awful.tag({1,2,3,4,5,6,7,8,9},1,layouts[1])
-- tags = {
--     names = {"dev", "web", "p2p", "im", "gimp", "6", "7", "8", "mm"},
--     layouts = {awful.layout.suit.max, 
--                 awful.layout.suit.max, 
--                 layouts[1], 
--                 awful.layout.suit.tile.right, 
--                 awful.layout.suit.tile.left,
--                 layouts[1],
--                 layouts[1],
--                 layouts[1],
--                 awful.layout.suit.max
--             }
-- }
-- tags[1] = awful.tag(tags.names,1,tags.layouts)
-- for i = 1, table.getn(tags[1]) do 
--     awful.tag.setproperty(tags[1][i], "persist", true)
-- end
-- dofile(awful.util.getdir("config") .. "/conf.d/rules.lua")
