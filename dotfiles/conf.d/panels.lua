-- {{{ Wibox
-- Create a textclock widget
--mytextclock = awful.widget.textclock({ align = "right" })

--dofile(os.getenv(XDG_CONFIG_HOME) .. "/awesome/conf.d/widgets.lua")

dofile(awful.util.getdir("config") .. "/conf.d/widgets.lua")

mytaglist.buttons = awful.util.table.join(
                    awful.button({ }, 1, awful.tag.viewonly),
                    awful.button({ modkey }, 1, awful.client.movetotag),
                    awful.button({ }, 3, awful.tag.viewtoggle),
                    awful.button({ modkey }, 3, awful.client.toggletag),
                    awful.button({ }, 4, awful.tag.viewnext),
                    awful.button({ }, 5, awful.tag.viewprev)
                    )
mytasklist = {}
mytasklist.buttons = awful.util.table.join(
                     awful.button({ }, 1, function (c)
                                              if not c:isvisible() then
                                                  awful.tag.viewonly(c:tags()[1])
                                              end
                                              client.focus = c
                                              c:raise()
                                          end),
                     awful.button({ }, 3, function ()
                                              if instance then
                                                  instance:hide()
                                                  instance = nil
                                              else
                                                  instance = awful.menu.clients({ width=250 })
                                              end
                                          end),
                     awful.button({ }, 4, function ()
                                              awful.client.focus.byidx(1)
                                              if client.focus then client.focus:raise() end
                                          end),
                     awful.button({ }, 5, function ()
                                              awful.client.focus.byidx(-1)
                                              if client.focus then client.focus:raise() end
                                          end))

for s = 1, screen.count() do
    -- Create a promptbox for each screen
    mypromptbox[s] = awful.widget.prompt({ layout = awful.widget.layout.horizontal.leftright, prompt = '<span color="#a83579">Run: </span>' })
    -- Create an imagebox widget which will contains an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    mylayoutbox[s] = awful.widget.layoutbox(s)
    mylayoutbox[s]:buttons(awful.util.table.join(
                           awful.button({ }, 1, function () awful.layout.inc(layouts, 1) end),
                           awful.button({ }, 3, function () awful.layout.inc(layouts, -1) end),
                           awful.button({ }, 4, function () awful.layout.inc(layouts, 1) end),
                           awful.button({ }, 5, function () awful.layout.inc(layouts, -1) end)))
    -- Create a taglist widget
        mytaglist[s] = awful.widget.taglist(s, awful.widget.taglist.label.all, mytaglist.buttons)
    -- Create a tasklist widget
    mytasklist[s] = awful.widget.tasklist(function(c)
                                              return awful.widget.tasklist.label.currenttags(c, s)
                                          end, mytasklist.buttons)

    -- Create the wibox
    mywibox[s] = awful.wibox({ position = "top", screen = s, height = 18 })
    -- Add widgets to the wibox - order matters
    mywibox[s].widgets = {
        {
--            mylauncher,
--            mypromptbox[s],
            layout = awful.widget.layout.horizontal.leftright
        },
         mylayoutbox[s],
--        kbdcfg.widget,
        kbdwidget,
         
        mytextclock,
        s == 1 and mysystray or nil,
        mytasklist[s],
        layout = awful.widget.layout.horizontal.rightleft
    }

    mybottomwi[s] = awful.wibox({ position = "bottom", height = 16, screen = s })
    mybottomwi[s].widgets = {
        {
            mytaglist[s],
            layout = awful.widget.layout.horizontal.leftright 
        },
        {
            spacer,
            separator,
            spacer,
           memwidget,
           spacer,
           separator,
           spacer,
           cpuwidget,
           spacer,
           separator,
           spacer,
           network,
           spacer,
           separator,
           spacer,
--           mypromptbox,
--            mempercent,
            layout = awful.widget.layout.horizontal.leftright 
        },
        spacer,
        volume,
--       pympd,
        spacer,
        mympd,
        mypromptbox,

        layout = awful.widget.layout.horizontal.rightleft 

    }
end
-- }}}

