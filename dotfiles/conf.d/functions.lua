function run_once(prg, arg)
    if not prg then
        do return nil end
    end
    if not arg then
        awful.util.spawn_with_shell("x=" .. prg .. "; pgrep -u $USERNAME -x " .. prg .. " || (" .. prg .. ")")
    else
        awful.util.spawn_with_shell("x=" .. prg .. "; pgrep -u $USERNAME -x " .. prg .. " || (" .. prg .. " " .. arg  .. ")")
    end
end


function uzbl_prompt(prompt, text, fifo, command)
    if command then
        -- if a command prefix is provided
        command = command .. ' '
    else
        command = ""
    end

    awful.prompt.run({prompt='uzbl '..prompt, text=text},
    mypromptbox[mouse.screen].widget,
    function(input)
--        awful.util.spawn_with_shell(string.format("cat '%s%s' > %s", command, input, fifo))
        awful.util.spawn_with_shell(string.format("~/.config/uzbl/script/uzbl_dbg.sh %s %s %s", command, input, fifo))
    end,
    function(cmd, cur_pos, ncomp)
        -- get the url
        local urls = {}
        f = io.popen('awk \'{print $3}\' '.. os.getenv("HOME") ..  '/.local/share/uzbl/history | sort -u')
        for url in f:lines() do
            table.insert(urls, url)
        end
        f:close()
        -- abort completion under certain circumstances
        if #cmd == 0 or (cur_pos ~= #cmd + 1 and cmd:sub(cur_pos, cur_pos) ~= " ") then
            return cmd, cur_pos
        end
        -- match
        local matches = {}
        table.foreach(urls, function(x)
            if urls[x]:find(cmd:sub(1,cur_pos)) then
                table.insert(matches, urls[x])
            end
        end)
        -- if there are no matches
        if #matches == 0 then
            return
        end
        -- cycle
        while ncomp > #matches do
            ncomp = ncomp - #matches
        end
        -- return match and position
        return matches[ncomp], cur_pos
    end)
end
