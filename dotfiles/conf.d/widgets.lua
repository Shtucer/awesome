-- {{{ Set an icon
function set_icon(image_file, align) 
  if align == nil then
    align = "right"
  end

  icon = widget({
    type = "imagebox",
    align = align
  })

  icon.image = image(image_file)
  icon.resize = false
  icon.valign = "center"
 
  return icon
end
-- }}}


spacer = widget({ type = "textbox", align = "right" })
spacer.text = " "
separator = widget({ type = "textbox", align = 'right' })
separator.text="|"

mytextclock = widget({ type = "textbox" })




-- Create a systray
mysystray = widget({ type = "systray" })

-- Create a wibox for each screen and add it
-- Keyboard map indicator and changer
    kbdcfg = {}
    kbdcfg.cmd = "setxkbmap"
    kbdcfg.layout = { { "us,ru", "" }, { "ru,us", "" } }
    kbdcfg.colors = { "#f8a539", "#79A835" }
    kbdcfg.current = 1  -- us is our default layout
    kbdcfg.widget = widget({ type = "textbox", align = "right" })
    kbdcfg.widget.text = ' <span color="' .. kbdcfg.colors[1] .. '" font_family="Ubuntu Mono" weight="ultrabold">' .. string.upper(string.gsub(kbdcfg.layout[kbdcfg.current][1], "(%w+),(%w+)", "%1", 1)) .. '</span> '
    kbdcfg.switch = function ()
       kbdcfg.current = kbdcfg.current % #(kbdcfg.layout) + 1
       local t = kbdcfg.layout[kbdcfg.current]
       local cur = string.upper(string.gsub(t[1], "(%w+),(%w+)", "%1", 1))
       kbdcfg.widget.text = ' <span color="' .. kbdcfg.colors[kbdcfg.current] .. '"  font_family="Ubuntu Mono" weight="heavy">' .. cur .. '</span> '
       os.execute( kbdcfg.cmd .. " " .. t[1] .. " " .. t[2] )
    end

    -- Mouse bindings
    kbdcfg.widget:buttons(awful.util.table.join(
        awful.button({ }, 1, function () kbdcfg.switch() end)
    ))

--
 
mywibox = {}
mybottomwi = {}
mypromptbox = {}
mylayoutbox = {}
mytaglist = {}
require("math")
function hook_timer()
--   mytextclock.text = os.date("%a %d %b, %H:%M")
    f = io.popen("date +'%a %d %b, %H:%M'")
    for t in f:lines() do
        mytextclock.text = t
    end
end
hook_timer()
--awful.hooks.timer.register(10, hook_timer)
hooks.timer(10,hook_timer)


-- MPD widget.
-- Show state mpd and current song
-- progressbar
require("vicious")
local unicode = require("unicode")
local utf8 = unicode.utf8
local utf8 = string
mympd = widget({type = "textbox", align=right})
vicious.register(mympd, vicious.widgets.mpd, 
    function (widget, args)
            state = args["{state}"]
            if args["{state}"] == "Play" then
                state = "▶"
            end
            if args["{state}"] == "Stop" then
                state = "◼"
            end
            if args["{state}"] == "Pause" then
                state = "▷"
            end
           ret = '<span color="#79A835">' .. state .. '</span> '
           track = args["{Artist}"] .. ' - ' .. args["{Title}"]
           if args["{time}"] then
               cur_pos = args["{time}"][1] or 100
               elapse = args["{time}"][2] or 1
           else
               cur_pos = 100
               elapse = 1
           end
           perc = cur_pos / elapse
           len = utf8.len(track)
           nhl= math.modf(len*perc) or len
           hlited = utf8.sub(track,1,nhl)
           nohlited = utf8.sub(track,-(len-nhl))
           ret = ret .. '<span color="#79A835">' .. hlited
           ret = ret .. '</span>' .. nohlited
           --.. '['..len..'  '..nhl .. '  ' ..hlited..' / '.. nohlited.. ']'
           return ret

     end
        , nil, nil)

mympd:buttons(awful.util.table.join(
        awful.button({ }, 1, function () awful.util.spawn("mpc toggle") end)
        ))

-- ▶
require("wicked")
-- Memory usage monitor
memw = {memmenu=nil, memwidget=nil}
memwidget = widget({ type = 'textbox', align = 'right' })
--wicked.register(memwidget, wicked.widgets.mem, '| mem: <span color="#3579A8">$1%</span> ($2M)')
vicious.register(memwidget, vicious.widgets.mem,  
    function (widget, args)
        str = '<span color="#3579A8">mem: </span> '
        if args[1] > 80 then
            str = str .. '<span color="#a83579">'
        else
            str = str .. '<span>'
        end
        return str .. args[1] .. '%</span>'
    end
    ,nil,nil)
memw.widget = memwidget

memw.widget:buttons(awful.util.table.join(
    awful.button({}, 1, function() 
        if memw.memmenu ~= nil then
            memw.memmenu:hide()
            memw.memmenu = nil
        else
            memw.memmenu = show_process_menu('%mem')
            memw.memmenu:show()
        end

    end)
))
cpuw = {cpumenu=nil,cpuwidget = nil}
cpuwidget = widget({ type = 'textbox', align = 'right'})
vicious.register(cpuwidget, vicious.widgets.cpu,
    function (widget, args)
        str = '<span color="#3579A8">cpu: </span> '
        if args[1] > 80 then
            str = str .. '<span color="#a83579">'
        else
            str = str .. '<span>'
        end
        return str .. args[1] .. '%</span>'
    end
    ,nil,nil)
cpuw.cpuwidget = cpuwidget

--Utility function
--split string to array
--TODO: add delimeter param
function split(str)
    ret = {}
    for token in string.gmatch(str,"[^%s]+") do
        table.insert(ret,token)
    end
    return ret
end


-- Callback function for mem and cpu widgets
-- show menu with top 10 process sorted by %sort{mem,cpu} param
-- clickin on process will kill him
function show_process_menu(sort)
    process = io.popen('ps ahxk -' .. sort .. ' -o "%cpu,comm,pid"')
    i = 0
    process_list = {}
    for p in process:lines() do
        local info = split(p)
        table.insert(process_list, { info[2] .. "\t" .. info[3], function() 
                awful.util.spawn_with_shell('kill ' .. info[3])
            end
        })
        i = i + 1
        if i == 10 then
            break
        end
    end
    process:close()
    
    process_menu = awful.menu({items=process_list})
    return process_menu
end


cpuw.cpuwidget:buttons(awful.util.table.join(
    awful.button({}, 1, function() 
        if cpuw.cpumenu ~= nil then
            cpuw.cpumenu:hide()
            cpuw.cpumenu = nil
            print("remove")
        else
            cpuw.cpumenu = show_process_menu('%cpu')
            cpuw.cpumenu:show()
            print("Create")
        end

    end)
))

volume = widget({ type = "textbox", align = 'left' })
vicious.register(volume, vicious.widgets.volume, 
    function (widget, args)
        if args[2] == "♩" then
            return '[<span color="#A83579">' .. args[1] .. '</span>]'
        else
            return '[<span color="#79A835">' .. args[1] .. '</span>]'
        end
    end,
        1, "Master -c 0")

    volume:buttons(awful.util.table.join(
        awful.button({ }, 1, function () awful.util.spawn("amixer -c 0 sset PCM toggle-mute") end)
    ))


-- Network widget
-- Show up- and downlink. Tooltip with adapter properties
network = widget({ type = "textbox", align = 'left'})
vicious.register(network, vicious.widgets.net, function(widget, args) 
    str = ""
    if args["{eth1 carrier"] == 1 then
        str = str .. '<span color="#A83579">'
    else
        str = str .. '<span color="#79A835">'
    end
    str = str .. 'eth1: </span>'
    str  = str .. '<span color="#3579A8">↑</span>' .. args['{eth1 up_kb}'] .. 'kb'


    str  = str .. ' <span color="#3579A8">↓</span>' .. args['{eth1 down_kb}'] .. 'kb'
    return str
end)

network_tooltip = awful.tooltip({
    timer_function = function()
        info = io.popen('ifconfig eth1')
        str = '\n'
        for l in info:lines() do
            if string.len(l) ~= 0 then
                str = str .. l .. '\n'
            end
        end
        return '<span color="#3579A8">'..str..'</span>'
    end
})
network_tooltip:add_to_object(network)
network_tooltip:set_timeout(5)

