require("skype.handler")

shifty.config.apps = {
    { match = {  }, buttons = {
                             awful.button({ }, 1, function (c) client.focus = c; c:raise() end),
                             awful.button({ modkey }, 1, function (c) awful.mouse.client.move() end),
                             awful.button({ modkey }, 3, awful.mouse.client.resize ), }, },
    { match = { "gimp%-image%-window" }, geometry = {175,15,1250,1170}, border_width = 0, tag = "gimp"},
    { match = { "^gimp%-toolbox$" }, geometry = {0,15,175,1170}, slave = true, border_width = 0, tag = "gimp" },
    { match = { "^gimp%-dock$" }, geometry = {1425,15,175,1170}, slave = true, border_width = 0, tag = "gimp" },
    { match = { "Thunderbird","Navigator","Vimperator","Pentadactyl","Gran Paradiso","Firefox","Iceweasel","vimprobable2", "Uzbl", "Chromium", "luakit"}, tag = "web"},
    { match = { "Google%-chrome"}, tag = "web" },
    { match = { "Gvim","xterm", "urxvt"} , honorsizehints = false, tag = "dev" },
--    { match = { ".Skype™ (Beta)$", "Pidgin"}, tag = "im" },
--    { match = { "Skype™ (Beta)$"}, tag = "im" },
    { match = {  "Skype™.*(Beta)"}, tag = "im", slave = false},
    { match = {  "Skype™.*чат"}, tag = "im", slave = true, run = function(c) --[[skype.handler.handle(c)]] tablist.new(c, "Skype") end},
--    { match = { class = "Skype", name = "Skype™ (Beta)$"}, tag = "dev", slave = true},
--    { match = {class = "Skype",  "Chats"},  tag = "im", slave = true },
--    { rule = {class = "Skype"}, properties = {tag = "im"} },
--    { rule = {class = {"Gvim", "geany"}}, properties = { tag = shifty.config.tags["dev"]}},
    { match = {"Banshee", "Sonata", "Exaile", "Deadbeef"}, tag = "mm"},
    { match = { "Eclipse","Netbeans","geany","emacs"}, tag="dev"},
    { match = { "transmission", "gftp", "Azuerus"}, tag = "p2p"}
--    { match = { "xterm"}, tag = "dev", run = function(c) tablist.new(c,"XTERM") end},
--    { match = {"Gvim"}, tag = "dev", run = function(c) tablist.new(c, "VIM") end}
}

