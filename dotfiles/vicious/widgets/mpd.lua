---------------------------------------------------
-- Licensed under the GNU General Public License v2
--  * (c) 2010, Adrian C. <anrxc@sysphere.org>
---------------------------------------------------

-- {{{ Grab environment
local tonumber = tonumber
local io = { popen = io.popen }
local setmetatable = setmetatable
local string = { gmatch = string.gmatch, format = string.format }
local helpers = require("vicious.helpers")
-- }}}
local function split(v,sep)
    local sep, fields = sep or ":", {}
    local pattern = string.format("([^%s]+)", sep)
    v:gsub(pattern, function(c) fields[#fields+1] = c and tonumber(c) end)
    return fields
end

    -- Mpd: provides Music Player Daemon information
    module("vicious.widgets.mpd")


    -- {{{ MPD widget type
    local function worker(format, warg)
        local mpd_state  = {
            ["{volume}"] = 0,
            ["{state}"]  = "N/A",
            ["{Artist}"] = "N/A",
            ["{Title}"]  = "N/A",
            ["{Album}"]  = "N/A",
            ["{Genre}"]  = "N/A",
            --["{Name}"] = "N/A",
            --["{file}"] = "N/A",
        }

        -- Fallback to MPD defaults
        local pass = warg and warg[1] or "\"\""
        local host = warg and warg[2] or "127.0.0.1"
        local port = warg and warg[3] or "6600"

        -- Construct MPD client options
        local mpdh = "telnet://"..host..":"..port
        local echo = "echo 'password "..pass.."\nstatus\ncurrentsong\nclose'"

        -- Get data from MPD server
        local f = io.popen(echo.." | curl --connect-timeout 1 -fsm 3 "..mpdh)

        for line in f:lines() do
            for k, v in string.gmatch(line, "([%w]+):[%s](.*)$") do
                if     k == "volume" then mpd_state["{"..k.."}"] = v and tonumber(v)
                elseif k == "state"  then mpd_state["{"..k.."}"] = helpers.capitalize(v)
                elseif k == "Artist" then mpd_state["{"..k.."}"] = helpers.escape(v)
                elseif k == "Title"  then mpd_state["{"..k.."}"] = helpers.escape(v)
                elseif k == "Album"  then mpd_state["{"..k.."}"] = helpers.escape(v)
                elseif k == "Genre"  then mpd_state["{"..k.."}"] = helpers.escape(v)
                elseif k == "repeat"  then mpd_state["{"..k.."}"] = v and tonumber(v)
                elseif k == "random"  then mpd_state["{"..k.."}"] = v and tonumber(v)
                elseif k == "single"  then mpd_state["{"..k.."}"] = v and tonumber(v)
                elseif k == "time"	then mpd_state["{"..k.."}"] = v and split(v, ":") 
                    --elseif k == "Name" then mpd_state["{"..k.."}"] = helpers.escape(v)
                    --elseif k == "file" then mpd_state["{"..k.."}"] = helpers.escape(v)
                end
            end
        end
        f:close()

        return mpd_state
    end
    -- }}}

    setmetatable(_M, { __call = function(_, ...) return worker(...) end })
